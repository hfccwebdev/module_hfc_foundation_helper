<?php

/**
 * @file
 * Primary module hooks for HFC Foundation Helper module.
 */

use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implements hook_form_alter().
 */
function hfc_foundation_helper_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if (preg_match('/^commerce_order_item_add_to_cart_form_commerce_product/', $form_id)) {
    $form['actions']['submit']['#submit'][] = '_hfc_foundation_helper_cart_redirect';
  }
}

/**
 * Form submit handler to redirect to cart.
 */
function _hfc_foundation_helper_cart_redirect(&$form, FormStateInterface $form_state) {

  $response = new RedirectResponse('/cart');

  // @see https://www.drupal.org/node/2023537
  $request = \Drupal::request();
  // Save the session so things like messages get saved.
  $request->getSession()->save();
  $response->prepare($request);
  // Make sure to trigger kernel events.
  \Drupal::service('kernel')->terminate($request, $response);

  $response->send();
}

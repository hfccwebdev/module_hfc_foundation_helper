<?php

namespace Drupal\hfc_foundation_helper\OrderProcessor;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\OrderProcessorInterface;
use Drupal\commerce_price\Price;

/**
 * Calculates donation price based on user-supplied order item field values.
 */
class DonationOrderProcessor implements OrderProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function process(OrderInterface $order) {
    foreach ($order->getItems() as $order_item) {
      if (!empty($order_item->field_other_donation_amount->value)) {
        $price = new Price(
          $order_item->field_other_donation_amount->value,
          $order->getStore()->getDefaultCurrencyCode()
        );
        $order_item->setUnitPrice($price, TRUE);
      }
      elseif (
        !empty($order_item->field_donation_amount->value) &&
        $order_item->field_donation_amount->value > 0
      ) {
        $price = new Price(
          (string) $order_item->field_donation_amount->value,
          $order->getStore()->getDefaultCurrencyCode()
        );
        $order_item->setUnitPrice($price, TRUE);
      }
    }
  }

}
